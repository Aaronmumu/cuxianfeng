//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    var API_URL = 'https://cuxianfeng.com/backend/site/login';
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    // wx.login({
    //   success: res => {
    //     // 发送 res.code 到后台换取 openId, sessionKey, unionId
    //   }
    // })
    function Login(code, encryptedData, iv) {
      console.log('code=' + code + '&encryptedData=' + encryptedData + '&iv=' + iv);
      //创建一个dialog
      wx.showToast({
        title: '正在登录...',
        icon: 'loading',
        duration: 10000
      });
      //请求服务器
      wx.request({
        url: API_URL,
        data: {
          wechat: '',
          code: code,
          encryptedData: encryptedData,
          iv: iv
        },
        method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
        header: {
          'content-type': 'application/json'
        }, // 设置请求的 header
        success: function (res) {
          // success
          wx.hideToast();
          console.log('服务器返回', res);

        },
        fail: function () {
          // fail
          // wx.hideToast();
        },
        complete: function () {
          // complete
        }
      })
    }

    wx.login({
      success: function (res) {
        if (res.code) {
          console.log('获取用户code成功！',res)
          var code = res.code;
          wx.getUserInfo({//getUserInfo流程
            success: function (res2) {//获取userinfo成功
              console.log(res2);
              var encryptedData = encodeURIComponent(res2.encryptedData);//一定要把加密串转成URI编码
              var iv = res2.iv;
              //请求自己的服务器
              //Login(code, encryptedData, iv);
            }
          })
        } else {
          console.log('获取用户登录态失败！' + res.errMsg)
        }
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  globalData: {
    userInfo: null
  }
})