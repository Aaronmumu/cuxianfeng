/*
 * 首页 navnav 数据
 */
function getIndexNavData() {
  var arr = [
    {
      id: 1,
      icon: "../../pages/image/mine_yaoqing.png",
      title: "特惠吃",
      url:"../index/eat/eat"
    },
    {
      id: 2,
      icon: "../../pages/image/mine_yaoqing.png",
      title: "进口优选",
      url: "../index/import/import"
    },
    {
      id: 3,
      icon: "../../pages/image/mine_yaoqing.png",
      title: "拼单",
      url: "../logs/logs"
    },
    {
      id: 4,
      icon: "../../pages/image/mine_yaoqing.png",
      title: "现金挑战",
      url: "../logs/logs"
    },
    {
      id: 5,
      icon: "../../pages/image/mine_yaoqing.png",
      title: "微信红包",
      url: "../logs/logs"
    }
  ]
  return arr
}
/**
 * 特惠吃 
 */
function getTehuiData(){
  var arr = [
    {
      id: 1,
      icon: "../../../pages/image/mine_yaoqing.png",
      title: "特惠吃",
      url: "../../logs/logs"
    },
    {
      id: 2,
      icon: "../../../pages/image/mine_yaoqing.png",
      title: "进口优选",
      url: "../../logs/logs"
    },
    {
      id: 3,
      icon: "../../../pages/image/mine_yaoqing.png",
      title: "拼单",
      url: "../../logs/logs"
    },
    {
      id: 4,
      icon: "../../../pages/image/mine_yaoqing.png",
      title: "现金挑战",
      url: "../../logs/logs"
    },
    {
      id: 5,
      icon: "../../../pages/image/mine_yaoqing.png",
      title: "微信红包",
      url: "../../logs/logs"
    },
    {
      id: 6,
      icon: "../../../pages/image/mine_yaoqing.png",
      title: "微信红包",
      url: "../../logs/logs"
    }
  ]
  return arr
}
/**
 * 进口优选 
 */
function getImportData() {
  var arr = [
    {
      id: 1,
      icon: "../../../pages/image/mine_yaoqing.png",
      title: "特惠吃",
      url: "../../logs/logs"
    },
    {
      id: 2,
      icon: "../../../pages/image/mine_yaoqing.png",
      title: "进口优选",
      url: "../../logs/logs"
    },
    {
      id: 3,
      icon: "../../../pages/image/mine_yaoqing.png",
      title: "拼单",
      url: "../../logs/logs"
    },
    {
      id: 4,
      icon: "../../../pages/image/mine_yaoqing.png",
      title: "现金挑战",
      url: "../../logs/logs"
    }
  ]
  return arr
}

module.exports = {
  getIndexNavData: getIndexNavData,
  getTehuiData: getTehuiData,
  getImportData: getImportData
}