 /*我的导航*/
function getMinenavData() {
	var arr = [
		{
			id: 1,
			icon: "../../../pages/image/mine_yaoqing.png",
			title: "我的钱包",
			url:"../logs/logs"
		},
		{
			id: 2,
			icon: "../../../pages/image/mine_nav/mine_fxzq.png",
			title: "分享赚钱",
			url:"../logs/logs"
		},
		{
			id: 3,
			icon: "../../../pages/image/mine_nav/mine_yqhy.png",
			title: "邀请好友",
			url:"../logs/logs"
		},
		{
			id: 4,
			icon: "../../../pages/image/mine_nav/mine_hhr.png",
			title: "平台合伙",
			url:"../logs/logs"
		}
	]
	return arr
}

function getMinenavData2() {
	var arr = [
	{
	id: 5,
	icon: "../../../pages/image/mine_nav/mime-hb.png",
	title: "我的红包",
	url:"../logs/logs"
    },
    {
      id: 6,
      icon: "../../../pages/image/mine_nav/mine-sc.png",
      title: "我的收藏",
      url: "../logs/logs"
    },
    {
      id: 7,
      icon: "../../../pages/image/mine_nav/mine-zj.png",
      title: "我的足迹",
      url: "../logs/logs"
    },
    {
      id: 8,
      icon: "../../../pages/image/mine_nav/mine_dingdna.png",
      title: "我的订单",
      url: "../logs/logs"
    }
	]
	return arr
}
function getListnavData() {
	var arr = [
	{
	id: 1,
	icon: "../../../pages/image/mine_nav/mine_jifen.png",
	title: "积分兑换",
	url:"../logs/logs"
    },
    {
      id: 2,
      icon: "../../../pages/image/mine_nav/setting_taobao.png",
      title: "我的淘宝",
      url: "../logs/logs"
    },
    {
      id: 3,
      icon: "../../../pages/image/mine_nav/mine_xiaoxi.png",
      title: "消息通知",
      url: "../logs/logs"
    },
    {
      id: 4,
      icon: "../../../pages/image/mine_nav/mine_kefu.png",
      title: "客服中心",
      url: "../logs/logs"
    },
    {
      id: 5,
      icon: "../../../pages/image/mine_nav/mine_yijian.png",
      title: "意见反馈",
      url: "../logs/logs"
    },
    {
      id: 6,
      icon: "../../../pages/image/mine_nav/about_us.png",
      title: "关于我们",
      url: "../logs/logs"
    }
	]
	return arr
}

	

module.exports = {
  getMinenavData: getMinenavData,
  getMinenavData2: getMinenavData2,
  getListnavData : getListnavData
}