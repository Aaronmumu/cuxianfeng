// pages/index/search/sea_tail/sea_tail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // nav 初始化
    navTopItems: [
        {
          id: 1,
          icon: "../../images/nav_icon_01.png",
          title: "综合"
        },
        {
          id: 2,
          icon: "../../images/nav_icon_02.png",
          title: "有券"
        },
        {
          id: 3,
          icon: "../../images/nav_icon_03.png",
          title: "销量"
        },
        {
          id: 4,
          icon: "../../images/nav_icon_04.png",
          title: "价格"
        }
    ],
    curNavId: 1
  },
  //标签切换
  switchTab: function (e) {
    let id = e.currentTarget.dataset.id,
      index = parseInt(e.currentTarget.dataset.index)
    this.curIndex = parseInt(e.currentTarget.dataset.index)
    console.log(e)
    var that = this
    this.setData({
      curNavId: id,
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})