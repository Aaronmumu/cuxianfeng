var fileData = require('../../utils/data.js')
Page({
  data: {
    teHuiDatas: fileData.getImportData()
  },
  onLoad: function (options) {
    console.clear()
    console.log(this.teHuiDatas)
  },
  // 跳转至详情页
  navigateDetail: function (e) {
    console.log(e)
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    })
  },
  getLocation: function () {
    wx.navigateTo({
      url: '../location/location'
    })
  },
  bookTap: function () {
    wx.navigateTo({
      url: '../book/book'
    })
  }
})